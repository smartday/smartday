#ifndef EVENT_H
#define EVENT_H

#include <string>
#include <time.h>
#include <locale>

using namespace std;

enum eventType {tTask, tReminder, tService};
enum triggerType {atTime, eventStart, eventEnd};

/* A little meta TODO: remove TODOs for what has already been TODOne */

/* TODO: This whole class needs to be reworked. We need:
 * 1) Properties made private, with proper getters and setters and everything.
 * 2) type must be static const. Bare Event must have type = tService,
 *  Task is tTask, Reminder is tReminder.
 * 3) More constructors for constructor god! Something that would let us create events
 *  more easily, without setting properties one-by-one. Also, we need copy constuctors and assignment operator.
 * 4) Destructors must be virtual, even if they don't do anything.
 * 5) Make prerequsites into something workable, like std::list of pointers, or whatever you like.
 * 6) Remove the general toString function (QML does it better) and add functions like startDate_toString for
 *  all relevant properties. It must output strings like "11/12/2014 12:05" without newlines. Find out if it's
 *  possible/easy/smart to output priority as a string of actual star symbols ★★☆☆☆. If it is, make it to.
 * 7) Specify somehow that difficulty is int 1-10, and priority is int 0-5. You can make them enums if you like.
 * 8) Make timeToComplete into something workable.
 * 9) As I found out, it is not trivial or smart to expose a C++ enum to QML. So make a function that returns
 * eventType in some unambiguous format, like string.
 * 10) If something's ugly, make it beautiful. If something's bad, make it good.
 * 11) Look through the rest of the code and change everything that works with events to comply with all the above changes.
 *
 * And, please, check if you code compiles and runs before pushing.
 */

/* TODO:
 * Make setters for dates be able to accept and parse strings
 */
class Event {
protected:
    string name;
    string description;
    time_t startTime; // We can't say 'startDate', we need to keep hours/minutes too
    time_t endTime;
    eventType type;
    //Event* prereqisites; // How do we store those? Not as copies, right?
    //IDK how to implement them efficiently now. Let's chat and cut this for now
    //TODO: Let's make something out to implement this


public:
    virtual ~Event()
    { }
    Event(const Event & object)
    {
        name = object.getName();
        description = object.getDescription();
        startTime = object.getStartTime();
        endTime = object.getEndTime();
        type = object.getEventType();
    }

    string getName() const
    { return name;}
    void setName(string name)
    { this->name = name;}
    string getDescription() const
    { return description;}
    void setDescription(string description)
    { this->description = description;}
    time_t getStartTime() const
    { return startTime;}
    void setStartTime(time_t time)
    { this->startTime = time;}
    time_t getEndTime() const
    { return endTime;}
    void setEndTime(time_t time)
    { this->endTime = time;}
    string getEventTypeString() const;
    eventType getEventType() const
    { return type; }
    void setEventTypeByString(string type);
    void setEventType(eventType evnType)
    { type = evnType; }

    Event(eventType type=tService):
        name(""),description(""),startTime(0),endTime(0),type(type){}
    Event(string name, string descr, time_t startTime, time_t endTime, eventType type):
        name(name),description(descr),startTime(startTime), endTime(endTime),type(type){}
};

class Task : public Event {
private:
    int priority;
    int timeToComplete;
    int difficulty;
    time_t deadline;
public:
    Task():Event(tTask),priority(0),timeToComplete(0),difficulty(0),deadline(0)
    {}
    Task(const Task &object):Event(object)
    {
        priority = object.getPriority();
        timeToComplete = object.getTimeToComplete();
        difficulty = object.getDifficulty();
        deadline = object.getDeadline();
    }

    void setPriority(int prior)
    { priority = prior;}
    int getPriority() const
    { return priority;}
    void setTimeToComplete(int length)
    { timeToComplete = length; }
    int getTimeToComplete() const
    { return timeToComplete; }
    int getDifficulty() const
    { return difficulty; }
    void setDifficulty(int diff)
    { difficulty = diff; }
    void setDeadline(time_t deadlne)
    { deadline = deadlne; }
    time_t getDeadline() const
    { return deadline; }
};


class Reminder : public Event {
protected:
    triggerType trigger;
public:
    Reminder(string description, time_t time):Event(tReminder),trigger(atTime)
    {
        description = description;
        startTime = time;
        endTime = time;
    }
    string getTriggerTypeString() const;
    triggerType getTriggerType() const
    { return trigger; }
    void setTriggerTypeByString(string type);
    void setTriggerType(triggerType trig)
    { trigger = trig; }

};

#endif
