#ifndef CALENDAR_H
#define CALENDAR_H

#include "Event.h"
#include "Planner.h"
#include <list>
#include <qqmlfile.h>
#include <QObject>
#include <QAbstractListModel>
#include <QDateTime>

class EventModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum EventRoles {
        //-Event-
        NameRole = Qt::UserRole + 1,
        DescriptionRole,
        StartTimeRole,
        EndTimeRole,
        TypeRole,
        PrerequisiteRole,
        //-Task-
        PriorityRole,
        TimeToCompleteRole,
        DifficultyRole,
        DeadlineRole,
        //-Reminder-
        TriggerRole
    };
    EventModel(QObject *parent = 0);
    void addEvent(Event* event);    
    void removeEvent(Event* event);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex());

    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole); //timujin: implement those.
    Qt::ItemFlags flags(const QModelIndex & index) const;

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Event*> m_events;
};



class Calendar : public QObject {
    Q_OBJECT

private:
    list<Event*> events;
    EventModel m_events;

    list<Event*> getAllUnplanned();
    list<Event> blockedTimeSlots();

public:
    Calendar();
    ~Calendar();

    Event* addEvent(Event);
    Task* addTask(Task);
    void removeEvent(Event*);

    list<Event*> getEvents();

    void plan(Event*); 					// when the user wants to plan a single event
    void plan(list<Event*>, time_t start=0, time_t finish=0);// when he wants to plan several or all events (magic would not work if we plan everything one-by-one)
                                // start and finish are for when he says 'plan the next week for me'
    void findJobNow();

    time_t stringToTime_t(string);  //service function for import
    string Time_tToChar(time_t);    //service function for export

    void importIcal(string file);
    void exportIcal(string file);

    void importTasks(string file);

    Q_INVOKABLE void m_add(QString name, QString desc, QDate startDate, QTime startTime, QDate endDate, QTime endTime);
    Q_INVOKABLE void m_remove(int index); // timujin: implement this
    Q_INVOKABLE EventModel& model();

    Q_INVOKABLE void reset_model(); // timujin: implement this
};

#endif
