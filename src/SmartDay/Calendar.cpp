﻿#include "Calendar.h"
#include <fstream>
#include <sstream>

EventModel::EventModel(QObject *parent):QAbstractListModel(parent) {}

void EventModel::addEvent(Event* event) {
    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    m_events << event;
    endInsertRows();
}

void EventModel::removeEvent(Event* event) {
    for(int i = 0; i < m_events.count(); i++)
        if (event == m_events[i])
            removeRow(i);
}

int EventModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return m_events.count();
}

QVariant EventModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= m_events.count())
        return QVariant();
    time_t tmpST, tmpET;
    const Event* event = m_events[index.row()];
    switch(role) {
        case NameRole: return QString((event->getName()).c_str());
        case DescriptionRole: return QString(event->getDescription().c_str());
        case StartTimeRole:
            tmpST = event->getStartTime();
            return QString(ctime(&tmpST)).simplified();
            break;
        case EndTimeRole:
            tmpET = event->getEndTime();
            return QString(ctime(&tmpET)).simplified();
        case TypeRole: return QString("tService");
        case PrerequisiteRole: return QString("pow");
        case PriorityRole: return 5;
        case TimeToCompleteRole: return 6; // TODO: Guess what
        case DifficultyRole: return 7;
        case DeadlineRole: return QString("pow");
        case TriggerRole: return QString("pow");
    }
    return QVariant();
}

QHash<int, QByteArray> EventModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[DescriptionRole] = "description";
    roles[StartTimeRole] = "startTime";
    roles[EndTimeRole] = "endTime";
    roles[TypeRole] = "type";
    roles[PrerequisiteRole] = "prerequisite";
    roles[PriorityRole] = "priority";
    roles[TimeToCompleteRole] = "timeToComplete";
    roles[DifficultyRole] = "difficulty";
    roles[DeadlineRole] = "deadline";
    roles[TriggerRole] = "trigger";
    return roles;
}

bool EventModel::removeRows(int row, int count, const QModelIndex & parent){
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    while (count--) delete m_events.takeAt(row);
    endRemoveRows();
    return true;
}

bool EventModel::setData(const QModelIndex & index, const QVariant & value, int role){
    if (index.row() < 0 || index.row() >= m_events.count())
        return false;

    const Event* event = m_events[index.row()];
    switch(role) { // TODO set value to corresponsing item.
        case NameRole: /*event->set name value)*/ emit dataChanged(index,index); return true;
        case DescriptionRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case StartTimeRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case EndTimeRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case TypeRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case PrerequisiteRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case PriorityRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case TimeToCompleteRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case DifficultyRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case DeadlineRole: /*event->set name value)*/emit dataChanged(index,index); return true;
        case TriggerRole: /*event->set name value)*/emit dataChanged(index,index); return true;
    }
    return false;
}

Qt::ItemFlags EventModel::flags(const QModelIndex & index) const {
    Q_UNUSED(index);
    return (Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
}

Calendar::Calendar(){
    /////////// Until I figure out how to add new items to model, I will test it on 2 made up Events
    time_t timer;
    time(&timer);
    Task evn/*("Kill Superman",
              "I need to kill Superman.",
              timer + 99999,
              timer + 109999,
              tService)*/;
    evn.setPriority(8);
    evn.setName("New task");
    this->addEvent(evn);
    ///////////// This code is not actually needed
}

Calendar::~Calendar(){
    list<Event*>::iterator it;
    for(it = events.begin(); it != events.end(); ++it)
        delete *it;
}

list<Event*> Calendar::getAllUnplanned() {
    list<Event*> result;
    list<Event*>::iterator it;
    for(it = events.begin(); it != events.end(); ++it)
        if (!(*it)->getStartTime())
            result.push_back(*it);
    return result;
}

list<Event> Calendar::blockedTimeSlots() {
    list<Event> result;
    list<Event*>::iterator it;
    for(it = events.begin(); it != events.end(); ++it) {
        Event slot(*(*it));
        result.push_back(slot);
    }
    return result;
}

Event* Calendar::addEvent(Event e) {
    Event* n = new Event;
    *n = e;
    events.push_back(n);
    m_events.addEvent(n);
    return n;
}

Task* Calendar::addTask(Task e) {
    Task* n = new Task;
    *n = e;
    events.push_back(n);
    m_events.addEvent(n); // Is't correct for Task?
    return n;
}

void Calendar::removeEvent(Event* e) {
    list<Event*>::iterator it;
    //int i = 0;
    for(it = events.begin(); it != events.end(); ++it)
        if ((*it) == e) {            
            m_events.removeEvent(*it);
            //delete *it; // removeEvents handles this (shut up)
            events.erase(it);
            return;
        }
    throw -1; // "Event does not exist" (no exception handling yet)
}

list<Event*> Calendar::getEvents() { return events; }

void Calendar::plan(Event* e) {
    /* TODO: it return bool: true if planning is sucessful, false otherwise.
     * Of course, make sure that planner returns false if planning failed. */
    list<Event*>::iterator it;
    bool isInCalendar = false;
    for(it = events.begin(); it != events.end(); ++it)
        if ((*it) == e)
            isInCalendar = true;
    if (!isInCalendar)
        throw -1; // "Event does not exist" (no exception handling yet)
    Planner::plan(e, blockedTimeSlots());
    if (!e->getStartTime())
        return/*return something that would let the GUI know that planning failed.*/;
    else
        return;
}

void Calendar::plan(list<Event*> e, time_t start/*=0*/, time_t finish/*=0*/) {
    list<Event*>::iterator it, it2;
    for(it2 = e.begin(); it2 != e.end(); ++it2) {
        bool isInCalendar = false;
        for(it = events.begin(); it != events.end(); ++it)
            if ((*it) == (*it2))
                isInCalendar = true;
        if (!isInCalendar)
            throw -1; // "Event does not exist" (no exception handling yet)
    }

    Planner::plan(e, start, finish, blockedTimeSlots());
    /* TODO: make Planner return list of events it did not plan sucessfully.
     * This function should return that list. */
}

void Calendar::findJobNow() {
    /* list<Event*> results =*/Planner::findJobNow(getEvents());
    /* return results; */
    // TODO: When Planner learns how to plan, uncomment this.
}

void Calendar::importIcal(string file) {
    string command, field, value;
    ifstream input (file.c_str());
    if (input.is_open()) {
        while ( getline (input,command) ) {
            if (command == "BEGIN:VEVENT") {
                Event env_cur;
                while ( getline (input, field, ':') ) {
                        getline ( input, value);
                        if ( field == "DTSTART") env_cur.setStartTime(stringToTime_t(value));
                        if ( field == "DTEND")   env_cur.setEndTime(stringToTime_t(value));
                        if ( field == "SUMMARY") env_cur.setName(value);
                        if ( field == "END" )    break;
                }
                addEvent(env_cur);
            }
        }
        input.close();
    }
}

void Calendar::importTasks(string file) {
    string command, field, value;
    ifstream input (file.c_str());
    if (input.is_open())
        while ( getline (input,value) ) {
            Task env_cur;
            env_cur.setName(value);
            env_cur.setDifficulty(rand()%10);
            env_cur.setPriority(rand()%10);
            addTask(env_cur);
         }
    input.close();
}

void Calendar::exportIcal(string file) {
    ofstream output (file.c_str());
    output << "BEGIN:VCALENDAR" << endl;
    list<Event*>::iterator it;
    for(it = events.begin(); it != events.end(); ++it) {
            output << "BEGIN:VEVENT" << endl;
            output << "DTSTART:" << Time_tToChar((*it)->getStartTime())<< endl;
            output << "DTEND:"   << Time_tToChar((*it)->getEndTime()) << endl;
            output << "SUMMARY:" << (*it)->getName() << endl;
            output << "END:VEVENT" << endl;
    }
    output << "END:VCALENDAR";
    output.close();
}


time_t Calendar::stringToTime_t(string value) {
    tm var;
    var.tm_year = atoi(value.substr(0,4).c_str())-1900;
    var.tm_mon  = atoi(value.substr(4,2).c_str()) - 1;
    var.tm_mday = atoi(value.substr(6,2).c_str());
    var.tm_hour = atoi(value.substr(9,2).c_str())+1;
    var.tm_min  = atoi(value.substr(11,2).c_str());
    var.tm_sec  = atoi(value.substr(13,2).c_str());
    return mktime(&var);
}

string Calendar::Time_tToChar(time_t date) {
    char buff[17];
    strftime(buff, 17, "%Y%m%dT%H%M%SZ", localtime(&date));
    string out(buff);
    return out;
}

void Calendar::m_add(QString name, QString desc, QDate startDate, QTime startTime, QDate endDate, QTime endTime)
{
    struct tm startTmStr;
    startTmStr.tm_hour = startTime.hour(); startTmStr.tm_min = startTime.minute();
    startTmStr.tm_year = startDate.year(); startTmStr.tm_mon = startDate.month();
    startTmStr.tm_mday = startDate.day();
    struct tm endTmStr;
    endTmStr.tm_hour = endTime.hour(); endTmStr.tm_min = endTime.minute();
    endTmStr.tm_year = endDate.year(); endTmStr.tm_mon = endDate.month();
    endTmStr.tm_mday = endDate.day();

    Event evn;

    evn.setName(name.toStdString());
    evn.setDescription(desc.toStdString());
    evn.setStartTime(mktime(&startTmStr));
    evn.setEndTime(mktime(&endTmStr));
    evn.setEventTypeByString("Service");
    addEvent(evn);
}
void Calendar::m_remove(int index) {
    list<Event*>::iterator it = events.begin();
    for(int i = 0; i < index; ++it, ++i);
    removeEvent(*it); // timujin: this is a working stub; make this good
}

EventModel& Calendar::model(){
    return m_events;
}

void Calendar::reset_model() {
    m_events.removeRows(0,m_events.rowCount());
    list<Event*>::iterator it;
    for(it = events.begin(); it != events.end(); ++it)
        m_events.addEvent(*it);
}
