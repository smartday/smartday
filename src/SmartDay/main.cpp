#include <QGuiApplication>
#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>
#include "Calendar.h"

int main(int argc, char ** argv)
{
    QGuiApplication app(argc, argv);

    Calendar theCalendar;
    theCalendar.importIcal("../schedule.ics");
    theCalendar.importTasks("../tasks");
    Planner plans;
    plans.findJobNow(theCalendar.getEvents());
    EventModel &model = theCalendar.model();

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    QQmlContext *ctxt = view.rootContext();
    ctxt->setContextProperty("theCalendar", &theCalendar);
    ctxt->setContextProperty("theCalendarEvents", &model);

    view.setSource(QStringLiteral("qml/SmartDay/main.qml"));
    view.show();

    return app.exec();
}
