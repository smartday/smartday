#include "Event.h"
#include <algorithm>

string Event::getEventTypeString() const
{
    switch (type)
    {
        case tService: return "Service";
        case tTask: return "Task";
        case tReminder: return "Reminder";
        default: return "";
    }
}
void Event::setEventTypeByString(string type)
{
    string lowerType = type;
    transform(lowerType.begin(), lowerType.end(), lowerType.begin(), ::tolower);
    if (lowerType == "service")
        type = tService;
    else if (lowerType == "task")
        type = tTask;
    else if (lowerType == "reminder")
        type = tReminder;
    else throw -1;
}

string Reminder::getTriggerTypeString() const
{
    switch (trigger)
    {
        case atTime: return "AtTime";
        case eventStart: return "EventStart";
        case eventEnd: return "EventEnd";
        default: return "";
    }
}
void Reminder::setTriggerTypeByString(string type)
{
    string lowerType = type;
    transform(lowerType.begin(), lowerType.end(), lowerType.begin(), ::tolower);
    if (lowerType == "attime")
        trigger = atTime;
    else if (lowerType == "eventstart")
        trigger = eventStart;
    else if (lowerType == "eventend")
        trigger = eventEnd;
    else throw -1;
}
