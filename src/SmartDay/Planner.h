#ifndef PLANNER_H
#define PLANNER_H

#include "Event.h"
#include <list>

using namespace std;

class Planner {
public:
    static void plan(list<Event*> events_to_plan, time_t start, time_t finish, list<Event> blocked_time_slots);
    static void plan(Event* event_to_plan, list<Event> blocked_time_slots);
    static list<Event*> findJobNow(list<Event*> events); // He will need everything for this - planned, unplanned, service events.
};

#endif
