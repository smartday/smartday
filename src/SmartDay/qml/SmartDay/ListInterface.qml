import QtQuick 2.0
import QtQuick.Controls 1.1

ListView {
    id: theList
    model: theCalendarEvents

    property int itemindex: -1
    spacing: 5
    delegate: EventShower {
        isSelected: index == theList.itemindex
        sh: EventDataContainer {
            _type: type
            _name: name
            descr: description
            startDate: startTime
            endDate: expireTime
            priority: priority
            difficulty: difficulty
            timeToComplete: timeToComplete
            deadlineDate: new Date(); //deadline
            trigger: trigger
        }
        MouseArea {
            anchors.top: parent.top
            width: parent.width
            height: 30
            onClicked: theList.itemindex == index ? theList.itemindex = -1 : theList.itemindex = index
        }

        onDeleteEvent: {
            theCalendar.m_remove(index)
        }
    }
}
