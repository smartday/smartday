import QtQuick 2.0
import QtQuick.Window 2.1
import QtQuick.Controls 1.0

Rectangle {
    id: mainWindow
    width: 800
    height: 600

    signal addEvent()

    Rectangle {
        id: interfacePicker
        color: "transparent"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10
        height: 25
        border.color: "black"
        border.width: 1
        radius: 5
        ExclusiveGroup {id: mainWindowType}
        RadioButton {
            id: listView

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 5
            width: parent.width / 2

            text: "List"
            exclusiveGroup: mainWindowType

            checked: true
        }
        RadioButton {
            id: calendarView

            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 5
            width: parent.width / 2

            text: "Calendar"
            exclusiveGroup: mainWindowType
        }
    }

    ListInterface {
        anchors.top: interfacePicker.bottom
        anchors.left: interfacePicker.left
        anchors.right: interfacePicker.right
        anchors.bottom: addButton.top
        anchors.margins: 10
        visible: listView.checked
    }

    CalendarInterface {
        anchors.top: interfacePicker.bottom
        anchors.left: interfacePicker.left
        anchors.right: interfacePicker.right
        height: Math.floor((addButton.y - y)/24)*24

        anchors.margins: 10
        visible: calendarView.checked
    }



    Button {
        id: addButton
        anchors.bottom: parent.bottom
        text: "Add"
        onClicked: {Qt.createComponent("AddEvent.qml").createObject(mainWindow, {});}
    }

    property EventDataContainer addedEvent
    onAddEvent: {
        theCalendar.m_add(addedEvent._name,
                          addedEvent.descr,
                          addedEvent.startDate,
                          addedEvent.startTime,
                          addedEvent.endDate,
                          addedEvent.endTime) // TODO: Make this able to add nonservice Events
    }
}
