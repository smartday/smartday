import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {
    id: me

    signal planEvent()
    signal editEvent()
    signal deleteEvent()

    property bool isSelected: false
    property EventDataContainer sh

    radius: 10
    width: parent.width
    height: isSelected ? 250 : 30
    color: "beige"
    border.color: "orange"
    border.width: 1

    Rectangle {
        id: title
        width: parent.width
        height: 30
        radius: 10
        border.color: "orange"
        border.width: 1
        color: isSelected? "moccasin" : "beige"
        anchors.top: parent.top
        anchors.left: parent.left

        Text {
            id: titleText
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.leftMargin: 5
            text: sh._name + sh._type
        }

        Button {
            id: planSingleEvent
            states {
                State {name: "Plan!"}
                State {name: "Go To!"}
            }
            state: sh.startDate.getFullYear() == 1970 ? "Plan!" : "Go to!"
            text: state
            width: 50
            height: 25
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            onClicked: sh.planEvent()
        }

        Text {
            id: plannedIndicator
            anchors.right: planSingleEvent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 10
            text: planSingleEvent.state == "Plan!" ? "(not planned yet)"
                                                   : "(planned for: " + sh.startDate + ")"
        }
    }

    Column {
        id: tServiceStats
        spacing: 5
        visible: sh._type == "tService" && me.isSelected
        anchors.left: parent.left
        anchors.top: title.bottom
        anchors.margins: 5
        Text {text: "No event data present."}
    }
    Column {
        id: tTaskStats
        spacing: 5
        visible: sh._type == "tTask" && me.isSelected
        anchors.left: parent.left
        anchors.top: title.bottom
        anchors.margins: 5
        Text {text: sh._type + (sh._type == "tTask")}
        Text {text: "Deadline: " + sh.deadlineDate}
        Text {text: "Priority: " + sh.priority}
        Text {text: "Difficulty: " + sh.difficulty}
        Text {text: "End: " + sh.endDate}
        Rectangle {width: 200; height: 50; color: "yellow"; Text{text:"I am prerequisite"}}
    }
    Column {
        id: tReminderStats
        spacing: 5
        visible: sh._type == "tReminder" && me.isSelected
        anchors.left: parent.left
        anchors.top: title.top
        anchors.margins: 5
        Text {text: "Trigger: " + sh.trigger}
        Rectangle {width: 200; height: 50; color: "yellow"; Text{text:"I am prerequisite"}}
    }

    TextArea {
        visible: me.isSelected
        text: sh.descr
        width: parent.width / 2
        height: 200
        readOnly: true
        anchors.right: parent.right
        anchors.top: title.bottom
        anchors.margins: 5
    }

    Button {
        id: editSingleEvent
        visible: me.isSelected
        text: "Edit"
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 5
        onClicked: me.editEvent()
    }
    Button {
        id: deleteSingleEvent
        visible: me.isSelected
        text: "Delete"
        anchors.bottom: parent.bottom
        anchors.left: editSingleEvent.right
        anchors.margins: 5
        onClicked: me.deleteEvent()
    }
}
