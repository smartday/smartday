import QtQuick 2.0

Item
{
    height: 50
    width: 50
    states: [
        State {name: "blank"},
        State {name: "selected"}
    ]
    state: "blank"
    Image
    {
        antialiasing: true
        source: parent.state == "blank" ? "../img/star_empty.png"
                                        : "../img/star_filled.png"
        anchors.fill: parent
    }
}

