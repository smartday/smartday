import QtQuick 2.0
import QtQuick.Controls 1.1

Row
{
    property int rating: 0
    property int temp_rating: ms5.containsMouse ? 5
                            : ms4.containsMouse ? 4
                            : ms3.containsMouse ? 3
                            : ms2.containsMouse ? 2
                            : ms1.containsMouse ? 1
                            : -1
    Star
    {
        state: temp_rating >= 1 || temp_rating == -1 && rating >= 1 ? "selected" : "blank"
        id: rating1
        MouseArea
        {
            id: ms1
            anchors.fill: parent
            hoverEnabled: true
            onClicked: rating !=1 ? rating = 1 : rating = 0
        }
    }
    Star
    {
        state: temp_rating >= 2 || temp_rating == -1 && rating >= 2 ? "selected" : "blank"
        id:rating2
        MouseArea
        {
            id: ms2
            anchors.fill: parent
            hoverEnabled: true
            onClicked: rating !=2 ? rating = 2 : rating = 0
        }
    }
    Star
    {
        state: temp_rating >= 3 || temp_rating == -1 && rating >= 3 ? "selected" : "blank"
        id:rating3
        MouseArea
        {
            id: ms3
            anchors.fill: parent
            hoverEnabled: true
            onClicked: rating != 3 ? rating = 3 : rating = 0
        }
    }
    Star
    {
        state: temp_rating >= 4 || temp_rating == -1 && rating >= 4 ? "selected" : "blank"
        id:rating4
        MouseArea
        {
            id: ms4
            anchors.fill: parent
            hoverEnabled: true
            onClicked: rating !=4 ? rating = 4 : rating = 0
        }
    }
    Star
    {
        state: temp_rating >= 5 || temp_rating == -1 && rating >= 5 ? "selected" : "blank"
        id:rating5
        MouseArea
        {
            id: ms5
            anchors.fill: parent
            hoverEnabled: true
            onClicked: rating !=5 ? rating = 5 : rating = 0
        }
    }
}
