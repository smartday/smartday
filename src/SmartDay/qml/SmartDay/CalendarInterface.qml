import QtQuick 2.0

Rectangle {
    id: theCalendar
    property int itemindex: -1

    color: "white"

    Item {
        id: mainGrid
        anchors.fill: parent

        Repeater {
            id: weekdayGrid
            model: 7

            anchors.fill: parent

            Rectangle {
                width: parent.width / 7
                height: parent.height
                color: "transparent"
                border.color: "black"
                border.width: 1
                opacity: 0.5

                x: index * parent.width / 7
                y: 0

                Repeater {
                    model: 24

                    anchors.fill: parent

                    Rectangle {
                        width: parent.width
                        height: parent.height / 24
                        color: "transparent"
                        border.color: "dark gray"
                        border.width: 0.5

                        x: 0
                        y: index * parent.height / 24
                    }
                }
            }

        }

        Repeater {
            id: eventBlocks

            anchors.fill: parent

            model: theCalendarEvents
            property int itemindex: -1

            delegate: EventBlock {
                sh: EventDataContainer {
                    _type: type
                    _name: name
                    descr: description
                    startDate: startTime
                    endDate: expireTime
                    priority: priority
                    difficulty: difficulty
                    timeToComplete: timeToComplete
                    deadlineDate: new Date() //deadline
                    trigger: trigger
                }

                currentMonday: {
                    var today = new Date();
                    today.setHours(0);
                    today.setMinutes(0)
                    today.setSeconds(0);
                    today.setMilliseconds(0);
                    return today;
                }
            }
        }

    }


}
