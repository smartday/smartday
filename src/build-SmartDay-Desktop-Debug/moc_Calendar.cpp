/****************************************************************************
** Meta object code from reading C++ file 'Calendar.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../SmartDay/Calendar.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Calendar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_EventModel_t {
    QByteArrayData data[1];
    char stringdata[12];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_EventModel_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_EventModel_t qt_meta_stringdata_EventModel = {
    {
QT_MOC_LITERAL(0, 0, 10)
    },
    "EventModel\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_EventModel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void EventModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject EventModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_EventModel.data,
      qt_meta_data_EventModel,  qt_static_metacall, 0, 0}
};


const QMetaObject *EventModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *EventModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EventModel.stringdata))
        return static_cast<void*>(const_cast< EventModel*>(this));
    return QAbstractListModel::qt_metacast(_clname);
}

int EventModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Calendar_t {
    QByteArrayData data[13];
    char stringdata[96];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Calendar_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Calendar_t qt_meta_stringdata_Calendar = {
    {
QT_MOC_LITERAL(0, 0, 8),
QT_MOC_LITERAL(1, 9, 5),
QT_MOC_LITERAL(2, 15, 0),
QT_MOC_LITERAL(3, 16, 4),
QT_MOC_LITERAL(4, 21, 4),
QT_MOC_LITERAL(5, 26, 9),
QT_MOC_LITERAL(6, 36, 9),
QT_MOC_LITERAL(7, 46, 7),
QT_MOC_LITERAL(8, 54, 7),
QT_MOC_LITERAL(9, 62, 8),
QT_MOC_LITERAL(10, 71, 5),
QT_MOC_LITERAL(11, 77, 5),
QT_MOC_LITERAL(12, 83, 11)
    },
    "Calendar\0m_add\0\0name\0desc\0startDate\0"
    "startTime\0endDate\0endTime\0m_remove\0"
    "index\0model\0reset_model\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Calendar[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    6,   34,    2, 0x02,
       9,    1,   47,    2, 0x02,
      11,    0,   50,    2, 0x02,
      12,    0,   51,    2, 0x02,

 // methods: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QDate, QMetaType::QTime, QMetaType::QDate, QMetaType::QTime,    3,    4,    5,    6,    7,    8,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Calendar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Calendar *_t = static_cast<Calendar *>(_o);
        switch (_id) {
        case 0: _t->m_add((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QDate(*)>(_a[3])),(*reinterpret_cast< QTime(*)>(_a[4])),(*reinterpret_cast< QDate(*)>(_a[5])),(*reinterpret_cast< QTime(*)>(_a[6]))); break;
        case 1: _t->m_remove((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->model(); break;
        case 3: _t->reset_model(); break;
        default: ;
        }
    }
}

const QMetaObject Calendar::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Calendar.data,
      qt_meta_data_Calendar,  qt_static_metacall, 0, 0}
};


const QMetaObject *Calendar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Calendar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Calendar.stringdata))
        return static_cast<void*>(const_cast< Calendar*>(this));
    return QObject::qt_metacast(_clname);
}

int Calendar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
