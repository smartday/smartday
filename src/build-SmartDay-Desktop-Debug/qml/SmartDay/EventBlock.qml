import QtQuick 2.0

Item {
    id: me
    property EventDataContainer sh
    property date currentMonday
    property date currentSundayEnd: new Date(currentMonday.getFullYear(),
                                             currentMonday.getMonth(),
                                             currentMonday.getDate() + 7,
                                             00, 00, 00)

    anchors.fill: parent

    property int dayWidth: parent.width / 7
    property int dayHeight: parent.height
    property int hourHeight: parent.height / 24

    Rectangle {
        id: theBlock

        color: sh._type == "tReminder" ?    "pink" :
               sh._type == "tTask" ?        "beige" :
                                        "light blue"
        radius: 10

        visible: currentMonday.getDate() <= sh.startDate.getDate() &&
                 currentSundayEnd.getDate() >= sh.startDate.getDate()

        x: dayWidth * (sh.startDate.getDate() - currentMonday.getDate()) + 1
        y: hourHeight * sh.startDate.getHours()
        width: dayWidth
        height: sh.endDate.getHours() - sh.startDate.getHours() == 0 ?
                    hourHeight :
                    hourHeight * (sh.endDate.getHours() - sh.startDate.getHours())

        Text {
            text: sh._name.slice(0,9) + "..."
        }
    }
}
