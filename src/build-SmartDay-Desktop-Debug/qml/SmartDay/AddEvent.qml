import QtQuick 2.0
import QtQuick.Controls 1.1

Item {
    id: dialogComponent

    anchors.fill: parent
    Rectangle {
        id: overlay
        anchors.fill: parent
        color: "black"
        opacity: 0.4
        MouseArea{
            anchors.fill: parent
        }
    }

    property EventDataContainer addedEvent: EventDataContainer {
        _type: eventTypePicker.state == "Task" ? "tTask"
                                              : "tReminder"

        // -Event-
        _name: eventNameField.text
        descr: eventDescriptionField.text
        startDate: _type == "tReminder" ? reminderDatePicker._date : new Date(1900);
        endDate:   _type == "tReminder" ? reminderDatePicker._date : new Date(1900);

        // -Task-
        priority: taskPriorityRating.rating
        difficulty: taskDifficultyPicker.value
        timeToComplete: 0
        deadlineDate: deadlineDatePicker._date

        // -Reminder-
        trigger: qsTr("Test")
    }

    Rectangle {
        anchors.centerIn:parent
        id: theWindow
        width: 350
        height: 425
        color: "beige"
        radius: 10
        border.color: "orange"
        border.width: 2

        Item {
            id: fields

            anchors.fill: parent
            anchors.margins: 5

            states: [
                State {name: "tTask"},
                State {name: "tReminder"},
                State {name: "tService"}
            ]
            state: eventTypePicker.currentText == "Task"        ? "tTask" :
                   eventTypePicker.currentText == "Reminder"    ? "tReminder"
                                                                : "tService"

            ComboBox {
                id: eventTypePicker

                width: parent.width/3
                anchors.left: parent.left
                anchors.margins: 5

                model: ListModel {
                    ListElement {text: "Task"}
                    ListElement {text: "Reminder"}
                    ListElement {text: "Other"}
                }
                currentIndex: 0
            }

            TextField {
                id: eventNameField

                width: parent.width
                anchors.top: eventTypePicker.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 5
                placeholderText: qsTr("Event name")
            }

            TextArea {
                id: eventDescriptionField

                width: parent.width
                height: 175
                text: "Enter description here..."
                anchors.top: eventNameField.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 5
            }

            Item {
                id: taskBundle

                visible: fields.state == "tTask"
                anchors.top: eventDescriptionField.bottom
                anchors.bottom: buttonBundle.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 5

                Rectangle {
                    id: taskPriority

                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: 300
                    height: 70

                    color: "crimson"
                    border.color: "red"
                    border.width: 2
                    radius: 10

                    StarRating
                    {
                        id: taskPriorityRating

                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.margins: 3

                    }
                    Label
                    {
                        anchors.bottom: taskPriority.bottom
                        anchors.horizontalCenter: taskPriorityRating.horizontalCenter
                        text: qsTr("Priority")
                    }
                }

                Item {
                    id: taskTime

                    width: 100
                    height: 25
                    anchors.top: taskPriority.bottom
                    anchors.left: parent.left
                    anchors.margins: 5

                    Button {
                        id: taskClock
                        width: 20
                        text: "O"
                        anchors.left: parent.left
                        anchors.top: parent.top
                    }

                    TextField {
                        id: taskText
                        width: 80
                        placeholderText: "2 hours"
                        anchors.left: taskClock.right
                        anchors.verticalCenter: taskClock.verticalCenter
                    }
                }

                Item {
                    id: taskDifficulty

                    anchors.left: taskTime.right
                    anchors.right: parent.right
                    anchors.top: taskPriority.bottom
                    anchors.margins: 5
                    height: taskTime.height

                    Slider {
                        id: taskDifficultyPicker
                        anchors.fill: parent
                        minimumValue: 1
                        maximumValue: 10
                        stepSize: 1
                    }
                    Label {
                        anchors.top: taskDifficultyPicker.bottom
                        anchors.horizontalCenter: taskDifficultyPicker.horizontalCenter
                        anchors.topMargin: -5
                        text: "Difficulty"
                    }
                }

                DatePicker {
                    id: deadlineDatePicker

                    title: "Deadline"

                    anchors.top: taskDifficulty.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.topMargin: 10
                    height: 25
                }
            }

            Item {
                id: reminderBundle

                visible: fields.state == "tReminder"
                anchors.top: eventDescriptionField.bottom
                anchors.bottom: buttonBundle.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 5

                Item {
                    id: reminderTriggerPick

                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 25

                    ExclusiveGroup {id: reminderTriggerType}
                    RadioButton {
                        id: triggerAtTime

                        anchors.top: parent.top
                        anchors.left: parent.left
                        width: parent.width / 3

                        text: "At time:"
                        exclusiveGroup: reminderTriggerType
                    }
                    RadioButton {
                        id: triggerEventStart

                        anchors.top: parent.top
                        anchors.left: triggerAtTime.right
                        width: parent.width / 3

                        text: "Before:"
                        exclusiveGroup: reminderTriggerType
                    }
                    RadioButton {
                        id: triggerEventEnd

                        anchors.top: parent.top
                        anchors.right: parent.right
                        width: parent.width / 3

                        text: "After:"
                        exclusiveGroup: reminderTriggerType
                    }
                }

                DatePicker {
                    id: reminderDatePicker

                    title: "When?"

                    visible: triggerAtTime.checked
                    anchors.top: reminderTriggerPick.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 25
                }

                Item {
                    id: reminderEventPicker

                    visible: triggerEventEnd.checked || triggerEventStart.checked
                }
            }

            Row {
                id: buttonBundle

                anchors.bottom: parent.bottom
                width: parent.width
                height: 25

                Button {
                    id: okPlan
                    text: "Plan"
                    width: 100
                }
                Button {
                    id: okSave
                    text: "Save"
                    width: parent.width - okPlan.width - okCancel.width
                    onClicked: {
                        dialogComponent.parent.addedEvent = dialogComponent.addedEvent
                        dialogComponent.parent.addEvent()
                        dialogComponent.destroy()
                    }
                }
                Button {
                    id: okCancel
                    text: "Cancel"
                    width: 80
                    onClicked: dialogComponent.destroy()
                }
            }
        }
    }
}
