import QtQuick 2.0

Item {
    property string _type

    // -Event-
    property string _name
    property string descr
    property date startDate
    property date endDate

    // -Task-
    property int priority
    property int difficulty
    property int timeToComplete
    property date deadlineDate

    // -Reminder-
    property string trigger
}
