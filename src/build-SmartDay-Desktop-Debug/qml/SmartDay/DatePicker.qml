import QtQuick 2.0
import QtQuick.Controls 1.0

Item {
    id: me

    property string title

    property date _date: upDate()

    function upDate() {
        var d = new Date(pickYear.text*1,
                         pickMonth.currentIndex,
                         pickDay.text*1,
                         pickHour.text*1,
                         pickMinute.text*1);
        return d;
    }
    Row {
        anchors.fill: parent

        Text {
            text: title
            width: parent.width / 6
        }

        TextField {
            id: pickDay

            width: parent.width / 6

            text: new Date().getDate()
            validator: IntValidator {bottom: 1; top: dayInMonth(_date.getMonth(), _date.getFullYear());}
            function dayInMonth(month, year) {
                return [31, ((year % 4 == 0) && (year % 400 != 0) ? 29 : 28),
                        31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
            }
        }

        ComboBox {
            id: pickMonth

            width: parent.width / 6

            model: ListModel {
                ListElement {text: "Jan"}
                ListElement {text: "Feb"}
                ListElement {text: "Mar"}
                ListElement {text: "Apr"}
                ListElement {text: "May"}
                ListElement {text: "Jun"}
                ListElement {text: "Jul"}
                ListElement {text: "Aug"}
                ListElement {text: "Sep"}
                ListElement {text: "Oct"}
                ListElement {text: "Nov"}
                ListElement {text: "Dec"}
            }
            currentIndex: new Date().getMonth()
        }

        TextField {
            id: pickYear

            width: parent.width / 6

            text: new Date().getFullYear()
            validator: IntValidator {bottom: 1991; top: 3000;}
        }

        TextField {
            id: pickHour

            width: parent.width / 6

            text: new Date().getHours()
            validator: IntValidator {bottom: 0; top: 23;}
        }

        TextField {
            id: pickMinute

            width: parent.width / 6

            text: "00"
            validator: IntValidator {bottom: 0; top: 59;}
        }
    }
}
